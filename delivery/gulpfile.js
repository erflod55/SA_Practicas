//Packages, modules and Libs
const gulp = require("gulp");
const del = require("del");
const zip = require("gulp-zip");

//Task to delete dist folder prev to build a new dist folder
gulp.task("del", () => {
  return del("dist/**", { force: true });
});

//Task to create dist folder
gulp.task("dist", () => {
  return gulp
    .src(["./**"], {
      base: "./",
      ignore: ["gulpfile.js", ".gitignore", "README.md", "delivery.zip", "dist"],
    })
    .pipe(gulp.dest("dist"));
});

//Task to compress dist folder
gulp.task("zip", () => {
  return gulp.src("./dist/**").pipe(zip("delivery.zip")).pipe(gulp.dest("./"));
});

//Array of task when build is running
gulp.task("build", gulp.series("del", "dist", "zip"));
