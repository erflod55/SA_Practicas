/**
 * Practica 2 - Software Avanzado
 * @author Erik Flores
*/
//Packages, modules and Libs
const express = require('express');
const dotenv = require('dotenv');
const deliveryRoute = require('./routes/delivery.route');
const registerESB = require('./utils/registerESB');
const app = express();

//Configure server
dotenv.config();
app.set('port', process.env.PORT || 3001);
app.use(express.json({limit : '50mb'}));
app.use(express.urlencoded({extended : true, limit : '50mb'}));

//Routes
app.use('/api/delivery', deliveryRoute);

//Register ESB
registerESB.register();

//Running server
let server = app.listen(app.get('port'), ()=>{
    console.log(`Server Delivery running on port ${app.get('port')}`);
});

module.exports = server;