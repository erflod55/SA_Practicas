const data = [];

module.exports.index = (req, res) => {
  res.send("Welcome to Delivery service");
};

module.exports.receiveOrder = (req, res) => {
  console.log('Orden recibida', req.body);
  data.push(req.body);
  res.send({ status: true, message: "Order received" });
};

module.exports.orderStatus = (req, res) => {
  const { _id } = req.params;
  console.log('Informar a cliente de su orden', _id);
  console.log(data[_id]);
  res.send(data[_id]);
};

module.exports.orderDelivered = (req, res) => {
  console.log('Orden entregada');
  const { _id } = req.params;
  data[_id].status = "Entregada";
  res.send(data[_id]);
};
