//Packages, modules and Libs
const express = require("express");
const router = express.Router();

//Controllers
const deliveryController = require('../controller/delivery.controller');

//Routes of Delivery Service
router.get('/', deliveryController.index);
router.get('/order/:_id', deliveryController.orderStatus);
router.post('/order', deliveryController.receiveOrder);
router.put('/order/:_id', deliveryController.orderDelivered);

module.exports = router;