/**
 * Server endpoints array
*/
const methods = [
    {
        method : 'GET',
        relativeURL : '/api/delivery/'
    },
    {
        method : 'GET',
        relativeURL : '/api/delivery/order/:_id'
    },
    {
        method : 'PUT',
        relativeURL : '/api/delivery/order/:_id'
    },
    {
        method : 'POST',
        relativeURL : '/api/delivery/order'
    }
];

exports.methods = methods;