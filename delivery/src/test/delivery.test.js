const request = require("supertest");
const server = require("../index");

describe("Delivery test", () => {
    it("Should be a status 200", async (done) => {
        const res = await request(server).get("/api/delivery/").send();
        expect(res.statusCode).toEqual(200);
        done();
    });

    it("Should be a new order", async (done) => {
        const res = await request(server).post("/api/delivery/order")
            .send({
                status: "Ordered",
                food: "Cheese Burger",
                client: "Erik Flores",
            });
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        done();
    });

    it('Should be a order created', async(done) => {
        const res = await request(server).get('/api/delivery/order/0').send();
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('food');
        expect(res.body).toHaveProperty('client');
        done();
    });

    it('Should be a order delivered', async(done) => {
        const res = await request(server).put('/api/delivery/order/0').send();
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('food');
        expect(res.body).toHaveProperty('client');
        expect(res.body.status).toEqual('Entregada');
        done();
    });
});
