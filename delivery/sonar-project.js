const sonarqubeScanner = require("sonarqube-scanner");

sonarqubeScanner(
  {
    serverUrl: "http://35.184.177.143:9000",
    options: {
      "sonar.sources": "src",
      "sonar.tests": "src",
      "sonar.inclusions": "**",
      "sonar.test.inclusions": "src/**/*.spec.js,src/**/*.spec.jsx,src/**/*.test.js,src/**/*.test.js",
      "sonar.javascript.lcov.reportPaths": "coverage/lcov.info",
      "sonar.testExecutionReportPaths": "coverage/test-reporter.xml",
    },
  },
  () => {}
);
