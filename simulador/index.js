const request = require("request");

function sleep(millis) {
    return new Promise((resolve) => setTimeout(resolve, millis));
}

async function createOrder(food, client) {
    const order = {
        status: "Ordered",
        food: food,
        client: client,
    };
    request.post(
        {
            headers: { "content-type": "application/json" },
            url: "http://localhost:3003/api/client/restaurant",
            body: JSON.stringify(order),
        },
        (error, response, body) => {
            console.dir(error || JSON.parse(body));
        }
    );
}

async function verificarRestaurant(id){
    request.get(
        {
            headers: { "content-type": "application/json" },
            url: "http://localhost:3003/api/client/restaurant/" + id,
        },
        (error, response, body) => {
            console.dir(error || JSON.parse(body));
        }
    );
}

async function verificarRepartidor(id){
    request.get(
        {
            headers: { "content-type": "application/json" },
            url: "http://localhost:3003/api/client/delivery/" + id,
        },
        (error, response, body) => {
            console.dir(error || JSON.parse(body));
        }
    );
}

async function pedidoListo(id){
    request.put(
        {
            headers: { "content-type": "application/json" },
            url: "http://localhost:3003/api/restaurant/order/" + id
        },
        (error, response, body) => {
            console.dir(error || JSON.parse(body));
        }
    );
}

async function pedidoEntregado(id){
    request.put(
        {
            headers: { "content-type": "application/json" },
            url: "http://localhost:3003/api/delivery/order/" + id,
        },
        (error, response, body) => {
            console.dir(error || JSON.parse(body));
        }
    );
}

async function main() {
    /* Solicitar dos pedidos */
    createOrder("Hamburguesa", "Erik Flores");
    await sleep(4000);
    
    createOrder("Pizza", "Gerardo Diaz");
    await sleep(4000);

    /* Verificar el estado de 1 pedido */
    verificarRestaurant(0);
    await sleep(4000);

    /* Pedido 1 Listo */
    pedidoListo(0);
    await sleep(4000);

    /* Verificar el estado de 1 pedido */
    verificarRepartidor(0);
    await sleep(4000);

    /* Pedido entregado */
    pedidoEntregado(0);
    await sleep(4000);

    /* Verificar el estado de 1 pedido */
    verificarRepartidor(0);
    await sleep(4000);

    /* Verificar estado de 1 pedido */
    verificarRestaurant(1);
}

main();