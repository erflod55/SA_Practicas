const request = require("request");

/**
 * The index route for Client Service
 */
module.exports.index = (req, res) => {
  res.send({ message: "Welcome to the client service" });
};

/**
 * this function receives an order and sents it to the restaurant service
 */
module.exports.createOrder = async (req, res) => {
  console.log("Orden Recibida: ", req.body);

  //Request to send order to Restaurant service
  await request.post(
    {
      headers: { "content-type": "application/json" },
      url: "http://localhost:3003/api/restaurant/order",
      body: JSON.stringify(req.body),
    },
    (error, response, body) => {
      console.dir(error || JSON.parse(body));
    }
  );
  res.send({ status: true, message: "Order created" });
};

/**
 * This function recieves an order id and returns the order status in the restaurant
 */
module.exports.restaurantStatus = async (req, res) => {
  console.log("Estado de orden en restaurante recibida");
  const { _id } = req.params;
  await request.get(
    {
      headers: { "content-type": "application/json" },
      url: `http://localhost:3003/api/restaurant/order/${_id}`
    },
    (error, response, body) => {
      console.dir(error || JSON.parse(body));
      res.send(body);
    }
  );
};

/**
 * This function recieves an order id and returns the order status in the delivery
 */
module.exports.deliveryStatus = async (req, res) => {
  console.log("Estado de orden en delivery recibida");
  const { _id } = req.params;
  await request.get(
    {
      headers: { "content-type": "application/json" },
      url: `http://localhost:3003/api/delivery/order/${_id}`
    },
    (error, response, body) => {
      console.dir(error || body);
      res.send(body);
    }
  );
};
