const request = require('request');
const methods = require('./methods').methods;
const url = 'http://localhost:3000';

/**
 * This function registers all service in the ESB
 */
exports.register = function(){
    request.post(
        {
          headers: { "content-type": "application/json" },
          url: "http://localhost:3003/esb/register",
          body: JSON.stringify({url : url, endpoints : methods}),
        },
        (error, response, body) => {
          console.dir(error || JSON.parse(body));
        }
      );
      console.log('Servicio registrado en el ESB');
}
