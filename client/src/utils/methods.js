/**
 * Server endpoints array
 */
const methods = [
    {
        method : 'GET',
        relativeURL : '/api/client/'
    },
    {
        method : 'GET',
        relativeURL : '/api/client/delivery/:_id'
    },
    {
        method : 'GET',
        relativeURL : '/api/client/restaurant/:_id'
    },
    {
        method : 'POST',
        relativeURL : '/api/client/restaurant'
    }
];

exports.methods = methods;