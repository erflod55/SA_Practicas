//Packages, modules and Libs
const express = require("express");
const router = express.Router();

//Controllers
const clientController = require("../controller/client.controller");

//Routes of Client Service
router.get("/", clientController.index);
router.get("/delivery/:_id", clientController.deliveryStatus)
router.get("/restaurant/:_id", clientController.restaurantStatus);
router.post('/restaurant', clientController.createOrder);

module.exports = router;