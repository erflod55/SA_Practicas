/**
 * Practica 3 - Software Avanzado
 * @author Erik Flores
*/

//Packages, modules and Libs
const express = require('express');
const dotenv = require('dotenv');
const clientRoute = require('./routes/client.route');
const app = express();
const registerESB = require('./utils/registerESB');

//Configure server
dotenv.config();
app.set('port', process.env.PORT || 3000);
app.use(express.json({limit : '50mb'}));
app.use(express.urlencoded({extended : true, limit : '50mb'}));

//Routes
app.use('/api/client', clientRoute);

//Register ESB
registerESB.register();

//Running server
app.listen(app.get('port'), ()=>{
    console.log(`Server Client running on port ${app.get('port')}`);
});