/**
 * Practica 3 - Software Avanzado
 * @author Erik Flores
*/
//Packages, modules and Libs
const express = require('express');
const dotenv = require('dotenv');
const restaurantRoute = require('./routes/restaurant.route');
const registerESB = require('./utils/registerESB');
const app = express();

//Configure server
dotenv.config();
app.set('port', process.env.PORT || 3002);
app.use(express.json({limit : '50mb'}));
app.use(express.urlencoded({extended : true, limit : '50mb'}));

//Routes
app.use('/api/restaurant', restaurantRoute);

//Register ESB
registerESB.register();

//Running server
app.listen(app.get('port'), ()=>{
    console.log(`Server Restaurant running on port ${app.get('port')}`);
});