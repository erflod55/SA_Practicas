//Packages, modules and Libs
const express = require("express");
const router = express.Router();

//Controllers
const restaurantController = require('../controllers/restaurant.controller');

//Routes of Delivery Service
router.get('/', restaurantController.index);
router.post('/order', restaurantController.receiveOrder);
router.get('/order/:_id', restaurantController.orderStatus);
router.put('/order/:_id', restaurantController.orderReady);

module.exports = router;