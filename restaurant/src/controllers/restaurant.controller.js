const request = require("request");
const data = [];

module.exports.index = (req, res) => {
  res.send("Welcome to Restaurant service");
};

module.exports.receiveOrder = (req, res) => {
  console.log("Orden recibida: ", req.body);
  data.push(req.body);
  res.send({ status: true, message: "Orden ingresada al restaurante" });
};

module.exports.orderStatus = (req, res) => {
  console.log("Estado de orden recibida");
  const { _id } = req.params;
  res.send(data[_id]);
};

module.exports.orderReady = async (req, res) => {
  console.log("Avisar a repartidor de su orden");
  const { _id } = req.params;
  data[_id].status = "Ready to delivery";
  request.post(
    {
      headers: { "content-type": "application/json" },
      url: "http://localhost:3003/api/delivery/order",
      body: JSON.stringify(data[_id]),
    },
    (error, response, body) => {
      console.dir(error || JSON.parse(body));
      res.send(data[_id]);
    }
  );
};
