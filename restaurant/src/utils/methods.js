/**
 * Server endpoints array
*/
const methods = [
    {
        method : 'GET',
        relativeURL : '/api/restaurant/'
    },
    {
        method : 'GET',
        relativeURL : '/api/restaurant/order/:_id'
    },
    {
        method : 'PUT',
        relativeURL : '/api/restaurant/order/:_id'
    },
    {
        method : 'POST',
        relativeURL : '/api/restaurant/order'
    }
];

exports.methods = methods;
