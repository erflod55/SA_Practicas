//Packages, modules and Libs
const express = require("express");
const router = express.Router();
const Record = require('../utils/record');
const records = [];

//Routes of ESB
router.post('/esb/register', (req, res)=>{
    const {url, endpoints} = req.body;
    console.log('Servicio ', url, ' a registrarse');
    for(let endpoint of endpoints){
        //Create a new Record and push the Route and Controller functions
        const record = new Record(endpoint.method, url, endpoint.relativeURL);
        record.createRoute(router);
        records.push(record);
    }
    console.log('Endpoints registrados: ', endpoints);
    res.send({ message : 'Enpoints registrados' });
});

module.exports = router;