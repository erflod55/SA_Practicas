/**
 * Practica 4 - Software Avanzado
 * @author Erik Flores
*/
//Package, modules and libs
const express = require('express');
const dotenv = require('dotenv');
const app = express();
const esbRoute = require('./routes/esb.route');

//Configure ESB
dotenv.config();
app.set('port', process.env.PORT || 3003);
app.use(express.json({limit : '50mb'}));
app.use(express.urlencoded({extended : true, limit : '50mb'}));

//Routes
app.use('', esbRoute);

//Running server
app.listen(app.get('port'), ()=>{
    console.log(`ESB running on port ${app.get('port')}`);
});