const apiAdapter = require('./apiAdapter');

class Record{

    /**
     * 
     * @param {string} method POST, GET, PUT
     * @param {string} service link
     * @param {string} relativeURL endpoint of the service
     */
    constructor(method, url, relativeURL){
        this.method = method;
        this.api = apiAdapter(url);
        this.relativeURL = relativeURL;
    }

    /**
     * Create a function, this function redirects the request to the service
     */
    getController(){
        switch (this.method) {
            case 'GET':
                return (req, res) =>{
                    console.log(this.relativeURL);
                    this.api.get(req.path).then(resp=>{
                        res.send(resp.data);
                    });
                };
            case 'POST':
                return (req, res) =>{
                    console.log(this.relativeURL);
                    this.api.post(req.path, req.body).then(resp=>{
                        res.send(resp.data);
                    });
                }
            case 'PUT':
                return (req, res) =>{
                    console.log(this.relativeURL);
                    this.api.put(req.path, req.body).then(resp=>{
                        res.send(resp.data);
                    });
                }
            default:
                console.error(`Not implemented ${this.method}`);
                return null;
        }
    }

    /**
     * This function create a endpoint
     * @param {Router} router 
     */
    createRoute(router){
        switch (this.method) {
            case 'GET':
                router.get(this.relativeURL, this.getController());
                break;
            case 'POST':
                router.post(this.relativeURL, this.getController());
                break;
            case 'PUT':
                router.put(this.relativeURL, this.getController());
                break;
            default:
                console.error(`Not implemented ${this.method}`);
                break;
        }
    }
}

module.exports = Record;