const axios = require('axios');

// Adapter from axios library to redirect requests
module.exports = (url) => {
    return axios.default.create({
        baseURL : url
    });
}