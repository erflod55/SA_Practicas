# ESB

## About
ESB de la Practica 4. Servidor corriendo en NodeJs en el puerto 3003.

## Tabla de Contenido

> * [Proyecto](#cliente)
>   * [About ](#about)
>   * [Tabla de contenido](#tabla-de-contenido)
>   * [Entorno de desarrollo](#entorno-de-desarrollo)
>       * [Herramientas y software](#herramientas-y-software)
>       * [Recursos del sistema](#recursos-del-sistema)
>   * [Configuración](#configuración)
>   * [Recursos](#recursos)
>   * [Contribuidores](#contribuidores)

## Entorno de desarrollo
### Herramientas y software
|Nombre|Version
|--|--|
|Node JS|12.18.3
|Npm|6.14.6

### Recursos del sistema
* **Sistema Operativo:** Windows 10 Pro
* **Memoria RAM:** 8 GB 
* **Procesador:** Intel Core i7 7th Generacion

## Configuración
El proyecto se encuentra en modo de desarrollo, para su uso unicamente se requiere tener instalado Nodejs en su versión 12.18.3 con el instalador de paquetes npm en su versión 6.14.6.

### Variables de Entorno
Por seguridad el token y la url de acceso no se encuentran en el repositorio y su uso se realiza mediante un archivo de configuracion de **.env**. 

Antes de ejecutar el proyecto es necesario crear este archivo en la raíz de este mismo, para ello se crea el archivo **.env.** y se agregan las siguientes estructura:
```txt
PORT=Puerto del servidor
```

### Ejecucion
Para ejecutar el proyecto unicamente se requiere de una consola. Los pasos para ejecutar el proyecto son los siguientes:
1. Clonar el repositorio: ``git clone https://github.com/erflod5/SA_Practicas``
2. Moverse a la rama Practica1: ``git checkout Practica4``
3. Moverse a la carpeta parte 2: ``cd esb``
3. Instalar los paquetes: ``npm install``
4. Ejecutar el comando: ``npm run dev``

## Diagrama
![Diagrama](./imgs/esb.jpg)

## Recursos
- [Node Js](https://nodejs.org/es/)
- [Request Package](https://www.npmjs.com/package/request)
- [Express](https://www.npmjs.com/package/express)

## Contribuidores
- [Erik Flores](https://github.com/erflod5) <br> 201701066
